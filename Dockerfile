FROM nextcloud:22

# Upgrade path to entrypoint.sh accordingly to nextcloud version
ADD https://github.com/nextcloud/docker/raw/master/22/apache/entrypoint.sh /

RUN mkdir /data ; \
    chmod 755 /entrypoint.sh ; \
    sed -i '/set -eu/a \\nchown -R www-data:root \/data\nchmod -R g=u \/data\n' /entrypoint.sh ; \
    sed -i 's|/var/www/html|/data|g' /entrypoint.sh ; \
    sed -i 's|/var/www/html|/data|g' /var/spool/cron/crontabs/www-data ; \
    sed -i 's|/var/www/html|/data|g' /etc/apache2/sites-enabled/000-default.conf ; \
    sed -i 's|Directory /var/www|Directory /data|g' /etc/apache2/apache2.conf

ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2-foreground"]
